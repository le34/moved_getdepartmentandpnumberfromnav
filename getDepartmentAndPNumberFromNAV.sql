DROP PROCEDURE UpdateEmployeePNumbers;

CREATE PROCEDURE UpdateEmployeePNumbers 
	AS
set nocount on
--Start med at tømme destinationstabellen
DELETE FROM [BusinessIntelligence].[dbo].[EmployeePNumbers] WHERE username IS NOT NULL;

--FØRST HENTES MEDARBEJDERE
--LMG Gruppen
INSERT INTO BusinessIntelligence.[dbo].EmployeePNumbers(username, department,pnumber) 
	SELECT t2.[No_] as [username], 
		         t2.[Global Dimension 1 Code] as [department], 
		         t2.[Production Unit No_] as [pnumber] 
                 FROM [NAV2017_PROD].[dbo].[LMG Gruppen$Employee] t2
	             WHERE t2.[Termination Date] = '1753-01-01 00:00:00.000'
				 ;

--LE34 Gruppen
INSERT INTO BusinessIntelligence.[dbo].EmployeePNumbers(username, department,pnumber) 
	SELECT t2.[No_] as [username], 
		         t2.[Global Dimension 1 Code] as [department], 
		         t2.[Production Unit No_] as [pnumber] 
                 FROM [NAV2017_PROD].[dbo].[LE34 Gruppen$Employee] t2
	             WHERE t2.[Termination Date] = '1753-01-01 00:00:00.000'
				 ;

--NB Gruppen
INSERT INTO BusinessIntelligence.[dbo].EmployeePNumbers(username, department,pnumber) 
	SELECT t2.[No_] as [username], 
		         t2.[Global Dimension 1 Code] as [department], 
		         t2.[Production Unit No_] as [pnumber] 
                 FROM [NAV2017_PROD].[dbo].[NB Gruppen$Employee] t2
	             WHERE t2.[Termination Date] = '1753-01-01 00:00:00.000'
				 ;

--LE34
INSERT INTO BusinessIntelligence.[dbo].EmployeePNumbers(username, department,pnumber) 
	SELECT t2.[No_] as [username], 
		         t2.[Global Dimension 1 Code] as [department], 
		         t2.[Production Unit No_] as [pnumber] 
                 FROM [NAV2017_PROD].[dbo].[Landinspektørfirmaet LE34$Employee] t2
	             WHERE t2.[Termination Date] = '1753-01-01 00:00:00.000'
				 AND [Global Dimension 1 Code] != 99
				 ;

--GEOTEAM
--LE34
INSERT INTO BusinessIntelligence.[dbo].EmployeePNumbers(username, department,pnumber) 
	SELECT t2.[No_] as [username], 
		         t2.[Global Dimension 1 Code] as [department], 
		         t2.[Production Unit No_] as [pnumber] 
                 FROM [NAV2017_PROD].[dbo].[GEOTEAM A_S$Employee] t2
	             WHERE t2.[Termination Date] = '1753-01-01 00:00:00.000'
				 ;


--DERNÆST HENTES RESSOURCER
INSERT INTO BusinessIntelligence.[dbo].EmployeePNumbers(username, department) 
	SELECT [No_] as [username], 
		         [Global Dimension 1 Code] as [department]
                 FROM [NAV2017_PROD].[dbo].[Landinspektørfirmaet LE34$Resource] t2
		         
	             WHERE t2.[Termination Date] = '1753-01-01 00:00:00.000'
				 AND [No_] NOT IN (SELECT username FROM BusinessIntelligence.[dbo].EmployeePNumbers)
				 AND [No_] NOT IN (SELECT No_ FROM [NAV2017_PROD].[dbo].[Landinspektørfirmaet LE34$Employee])
				 AND [Global Dimension 1 Code] != 99
				 order by department desc;

INSERT INTO BusinessIntelligence.[dbo].EmployeePNumbers(username, department) 
	SELECT [No_] as [username], 
		         [Global Dimension 1 Code] as [department]
                 FROM [NAV2017_PROD].[dbo].[GEOTEAM A_S$Resource] t2
		         
	             WHERE t2.[Termination Date] = '1753-01-01 00:00:00.000'
				 AND [No_] NOT IN (SELECT username FROM BusinessIntelligence.[dbo].EmployeePNumbers)
				 AND [No_] NOT IN (SELECT No_ FROM [NAV2017_PROD].[dbo].[GEOTEAM A_S$Employee])
				 AND [Global Dimension 1 Code] != 99
				 order by department desc;
set nocount off;
SELECT * FROM BusinessIntelligence.[dbo].EmployeePNumbers ORDER BY username;

--exec UpdateEmployeePNumbers

